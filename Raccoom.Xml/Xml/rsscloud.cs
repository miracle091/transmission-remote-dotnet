// Copyright � 2009 by Christoph Richner. All rights are reserved.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// website http://www.raccoom.net, email support@raccoom.net, msn chrisdarebell@msn.com

using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;

namespace Raccoom.Xml
{
    /// <summary>Is an optional sub-element of channel.  It specifies a web service that supports the rssCloud interface which can be implemented in HTTP-POST, XML-RPC or SOAP 1.1.</summary>
    [ComVisible(true), Guid("026FF54F-96DF-4879-A355-880832C49A1C")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("Raccoom.RssCloud")]
    [XmlType("cloud")]
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class RssCloud : IRssCloud
    {
        #region fields

        ///<summary>A PropertyChanged event is raised when a property is changed on a component. A PropertyChangedEventArgs object specifies the name of the property that changed.</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>Domain</summary>
        private string _domain;

        /// <summary>Port</summary>
        private int _port;

        /// <summary>Path</summary>
        private string _path;

        /// <summary>RegisterProcedure</summary>
        private string _registerProcedure;

        /// <summary>Protocol</summary>
        private CloudProtocol _protocol;

        #endregion fields

        #region constructors

        /// <summary>Initializes a new instance of RssCloud with default values</summary>
        public RssCloud()
        {
        }

        /// <summary>Initializes a new instance of RssCloud</summary>
        public RssCloud(XmlReader xmlTextReader)
        {
            if (!xmlTextReader.HasAttributes)
                return;

            PropertyInfo propertyInfo = null;

            while (xmlTextReader.MoveToNextAttribute())
            {
                // find related property by name
                propertyInfo = GetType().GetProperty(xmlTextReader.Name, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (propertyInfo != null)
                {
                    // Protocol enum needs some conversion before the typeconverter can set the values that contains - char
                    if (propertyInfo.Name == "Protocol")
                        propertyInfo.SetValue(this, TypeDescriptor.GetConverter(propertyInfo.PropertyType).ConvertFromString(xmlTextReader.ReadInnerXml().Trim().Replace("-", "")), null);
                    else
                        XmlSerializationUtil.DecodeXmlTextReaderValue(this, xmlTextReader);
                }
            }
        }

        #endregion constructors

        #region public interface

        /// <summary></summary>
        [Category("RssCloud"), Description("")]
        [XmlAttribute("domain")]
        public string Domain
        {
            get
            {
                return _domain;
            }

            set
            {
                bool changed = !Equals(_domain, value);
                _domain = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Domain));
            }
        }

        // end Domain

        /// <summary></summary>
        [Category("RssCloud"), Description("")]
        [XmlAttribute("port")]
        public int Port
        {
            get
            {
                return _port;
            }

            set
            {
                bool changed = !Equals(_port, value);
                _port = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Port));
            }
        }

        // end Port

        /// <summary>
        /// Instructs the XmlSerializer whether or not to generate the XML element
        /// </summary>
        [XmlIgnore]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public bool PortSpecified
        {
            get
            {
                return _port > 0;
            }

            set
            {
            }
        }

        /// <summary></summary>
        [Category("RssCloud"), Description("")]
        [XmlAttribute("path")]
        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                bool changed = !Equals(_path, value);
                _path = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Path));
            }
        }

        // end Path

        /// <summary></summary>
        [Category("RssCloud"), Description("")]
        [XmlAttribute("registerProcedure")]
        public string RegisterProcedure
        {
            get
            {
                return _registerProcedure;
            }

            set
            {
                bool changed = !Equals(_registerProcedure, value);
                _registerProcedure = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.RegisterProcedure));
            }
        }

        // end RegisterProcedure

        /// <summary></summary>
        [Category("RssCloud"), Description(""), DefaultValue(CloudProtocol.None)]
        [XmlAttribute("protocol")]
        public CloudProtocol Protocol
        {
            get
            {
                return _protocol;
            }

            set
            {
                bool changed = !Equals(_protocol, value);
                _protocol = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Protocol));
            }
        }

        // end Protocol

        /// <summary>
        /// Instructs the XmlSerializer whether or not to generate the XML element
        /// </summary>
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        [XmlIgnore]
        public bool ProtocolSpecified
        {
            get
            {
                return _protocol != CloudProtocol.None;
            }

            set
            {
            }
        }

        /// <summary>
        /// Obtains the String representation of this instance.
        /// </summary>
        /// <returns>The friendly name</returns>
        public override string ToString()
        {
            return Domain + Port + Path + RegisterProcedure;
        }

        #endregion public interface

        #region protected interface

        ///<summary>A PropertyChanged event is raised when a property is changed on a component. A PropertyChangedEventArgs object specifies the name of the property that changed.</summary>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        #endregion protected interface

        #region nested classes

        /// <summary>
        /// public writeable class properties
        /// </summary>
        internal struct Fields
        {
            public const string Domain = "Domain";
            public const string Port = "Port";
            public const string Path = "Path";
            public const string RegisterProcedure = "RegisterProcedure";
            public const string Protocol = "Protocol";
        }

        #endregion nested classes
    }
}