// Copyright � 2009 by Christoph Richner. All rights are reserved.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// website http://www.raccoom.net, email support@raccoom.net, msn chrisdarebell@msn.com

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Serialization;

namespace Raccoom.Xml
{
    /// <summary>Enclosure is an optional sub-element of item.</summary>
    [ComVisible(true), Guid("026FF52F-96DF-4879-A355-880832C49A1C")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("Raccoom.RssEnclosure")]
    [XmlType("enclosure")]
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class RssEnclosure : IRssEnclosure
    {
        #region fields

        ///<summary>A PropertyChanged event is raised when a property is changed on a component. A PropertyChangedEventArgs object specifies the name of the property that changed.</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>Url</summary>
        private string _url;

        /// <summary>Length</summary>
        private int _length;

        /// <summary>Type</summary>
        private string _type;

        #endregion fields

        #region constructors

        /// <summary>Initializes a new instance of RssEnclosure with default values</summary>
        public RssEnclosure()
        {
        }

        /// <summary>Initializes a new instance of RssEnclosure</summary>
        public RssEnclosure(XmlReader xmlTextReader)
        {
            Debug.Assert(xmlTextReader.HasAttributes);

            while (xmlTextReader.MoveToNextAttribute())
            {
                XmlSerializationUtil.DecodeXmlTextReaderValue(this, xmlTextReader);
            }
        }

        #endregion constructors

        #region public interface

        /// <summary> The url must be an http url.</summary>
        [Category("RssEnclosure"), Description(" The url must be an http url.")]
        [XmlAttribute("url", DataType = "anyURI")]
        public string Url
        {
            get
            {
                return _url;
            }

            set
            {
                bool changed = !Equals(_url, value);
                _url = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Url));
            }
        }

        // end Url

        /// <summary>length says how big it is in bytes,</summary>
        [Category("RssEnclosure"), Description("length says how big it is in bytes,")]
        [XmlAttribute("length")]
        public int Length
        {
            get
            {
                return _length;
            }

            set
            {
                bool changed = !Equals(_length, value);
                _length = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Length));
            }
        }

        // end Length

        /// <summary>
        /// Instructs the XmlSerializer whether or not to generate the XML element
        /// </summary>
        [XmlIgnore]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public bool LengthSpecified
        {
            get
            {
                return _length > 0;
            }

            set
            {
            }
        }

        /// <summary>type says what its type is, a standard MIME type.</summary>
        [Category("RssEnclosure"), Description("type says what its type is, a standard MIME type.")]
        [XmlAttribute("type")]
        public string Type
        {
            get
            {
                return _type;
            }

            set
            {
                bool changed = !Equals(_type, value);
                _type = value;
                if (changed)
                    OnPropertyChanged(new PropertyChangedEventArgs(Fields.Type));
            }
        }

        // end Type

        /// <summary>
        /// Obtains the String representation of this instance.
        /// </summary>
        /// <returns>The friendly name</returns>
        public override string ToString()
        {
            return Url;
        }

        #endregion public interface

        #region protected interface

        ///<summary>A PropertyChanged event is raised when a property is changed on a component. A PropertyChangedEventArgs object specifies the name of the property that changed.</summary>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        #endregion protected interface

        #region nested classes

        /// <summary>
        /// public writeable class properties
        /// </summary>
        internal struct Fields
        {
            public const string Url = "Url";
            public const string Length = "Length";
            public const string Type = "Type";
            public const string Path = "Path";
        }

        #endregion nested classes
    }
}