﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet
{
    public partial class AliasesDialog : Form
    {
        private List<StorageManager.StorageAlias> StorageAliases;
        public string SelectedAliasPath { get; set; }

        public AliasesDialog(List<StorageManager.StorageAlias> aliasesList)
        {
            InitializeComponent();
            StorageAliases = aliasesList;
            InitializeAliases();
        }

        private void InitializeAliases()
        {
            int xOffset = 0;
            int yOffset = 0;
            const int ImageWidth = 120;
            const int ImageHeight = 120;
            var count = StorageAliases.Count;
            for (var i = 0; i < count; ++i)
            {
                var alias = StorageAliases[i];
                var pb = new PictureBox();
                pb.Width = ImageWidth;
                pb.Height = ImageHeight;
                pb.SizeMode = PictureBoxSizeMode.StretchImage;
                pb.Image = StorageManager.Base64ToImage(alias.ImageBase64);
                pb.Left = xOffset;
                pb.Top = yOffset;
                pb.Tag = alias.Path;
                var tt = new ToolTip();
                tt.SetToolTip(pb, alias.Name + "\r\n" + alias.Path);
                pb.MouseClick += new MouseEventHandler(pb_MouseClick);
                Controls.Add(pb);
                if (i % 3 == 2)
                {
                    yOffset = ImageHeight;
                    xOffset = 0;
                }
                else
                {
                    xOffset = ImageWidth;
                }
            }
            SelectedAliasPath = "test";
        }

        private void pb_MouseClick(object sender, MouseEventArgs e)
        {
            var pb = sender as PictureBox;
            SelectedAliasPath = pb.Tag.ToString();
            Close();
        }
    }
}