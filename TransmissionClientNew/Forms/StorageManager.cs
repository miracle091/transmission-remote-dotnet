﻿using Jayrock.Json;
using Jayrock.Json.Conversion;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet
{
    public partial class StorageManager : Form
    {
        public class StorageAlias
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Path { get; set; }
            public string ImageBase64 { get; set; }

            public void Set(string id, string name, string path, Image image)
            {
                Id = id;
                Name = name;
                Path = path;
                var resizedImage = ResizeImage(image, new Size(240, 240));
                var base64Image = ImageToBase64(resizedImage, GetImageFormat(image));
                ImageBase64 = base64Image;
            }

            public void Set(string id, string name, string path, string imageBase64)
            {
                Id = id;
                Name = name;
                Path = path;
                ImageBase64 = imageBase64;
            }
        }

        private const string STORAGE_MANAGER_SETTINGS = @"storage_aliases.json";
        private List<StorageAlias> StorageAliases;
        private BindingSource aliasesBindingSource;
        private StorageAlias SelectedItem { get; set; }

        public StorageManager()
        {
            InitializeComponent();
            StorageAliases = LoadSettings();
            if (StorageAliases == null)
                StorageAliases = new List<StorageAlias>();

            PopulateListBox(aliasListBox, StorageAliases);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            SaveSettings();
            Close();
        }

        public static List<StorageAlias> LoadSettings()
        {
            try
            {
                using (FileStream inFile = new FileStream(STORAGE_MANAGER_SETTINGS, FileMode.Open, FileAccess.Read))
                {
                    byte[] binaryData = new byte[inFile.Length];
                    if (inFile.Read(binaryData, 0, (int)inFile.Length) < 1)
                    {
                        //throw new Exception(OtherStrings.EmptyFile);
                    }
                    var jsonArray = (JsonArray)JsonConvert.Import(Encoding.UTF8.GetString(binaryData));
                    return JsonArrayToStorageAliases(jsonArray);
                }
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        private void SaveSettings()
        {
            using (FileStream outFile = new FileStream(STORAGE_MANAGER_SETTINGS, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter writer = new StreamWriter(outFile))
                {
                    var json = new JsonArray(StorageAliases);
                    writer.Write(json.ToString());
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(aliasTextBox.Text) && !string.IsNullOrEmpty(pathTextBox.Text) && imagePictureBox.Image != null)
            {
                StorageAlias activeAlias;
                if (SelectedItem == null)
                {
                    activeAlias = new StorageAlias();
                    activeAlias.Set(Guid.NewGuid().ToString(), aliasTextBox.Text, pathTextBox.Text, imagePictureBox.Image);
                    StorageAliases.Add(activeAlias);
                }
                else
                {
                    activeAlias = SelectedItem;
                    activeAlias.Set(SelectedItem.Id, aliasTextBox.Text, pathTextBox.Text, imagePictureBox.Image);
                }
                PopulateListBox(aliasListBox, StorageAliases);
                SelectedItem = null;
            }
        }

        private bool _populating = false;

        private void PopulateListBox(ListBox lb, IList list)
        {
            _populating = true;
            aliasListBox.Items.Clear();
            aliasListBox.Items.AddRange(StorageAliases.ToArray());
            //lb.DataSource = list;
            lb.DisplayMember = "Name";
            lb.ValueMember = "Id";
            lb.SelectedIndex = -1;
            //aliasesBindingSource.ResetBindings(true);
            //aliasesBindingSource.Position = -1;
            _populating = false;
        }

        private void aliasListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!_populating)
            {
                var item = (sender as ListBox).SelectedItem as StorageAlias;
                if (item != null)
                {
                    aliasTextBox.Text = item.Name;
                    pathTextBox.Text = item.Path;
                    imagePictureBox.Image = Base64ToImage(item.ImageBase64);
                    SelectedItem = item;
                }
            }
        }

        private void imagePictureBox_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg, *.jpeg, *.png)|*.jpg;*.jpeg;*.png";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    imagePictureBox.ImageLocation = dlg.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private static List<StorageAlias> JsonArrayToStorageAliases(JsonArray jsonArray)
        {
            var list = new List<StorageAlias>();
            foreach (JsonObject jsonObject in jsonArray)
            {
                var storageAlias = new StorageAlias();
                storageAlias.Set(jsonObject["id"].ToString(), jsonObject["name"].ToString(), jsonObject["path"].ToString(), jsonObject["imageBase64"].ToString());
                list.Add(storageAlias);
            }
            return list;
        }

        #region Image helpers

        public static string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        private static Image ResizeImage(Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = size.Width / (float)sourceWidth;
            nPercentH = size.Height / (float)sourceHeight;

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage(b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return b;
        }

        public static ImageFormat GetImageFormat(Image img)
        {
            if (img.RawFormat.Equals(ImageFormat.Jpeg))
                return ImageFormat.Jpeg;
            if (img.RawFormat.Equals(ImageFormat.Bmp))
                return ImageFormat.Bmp;
            if (img.RawFormat.Equals(ImageFormat.Png))
                return ImageFormat.Png;
            if (img.RawFormat.Equals(ImageFormat.Emf))
                return ImageFormat.Emf;
            if (img.RawFormat.Equals(ImageFormat.Exif))
                return ImageFormat.Exif;
            if (img.RawFormat.Equals(ImageFormat.Gif))
                return ImageFormat.Gif;
            if (img.RawFormat.Equals(ImageFormat.Icon))
                return ImageFormat.Icon;
            if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
                return ImageFormat.MemoryBmp;
            if (img.RawFormat.Equals(ImageFormat.Tiff))
                return ImageFormat.Tiff;
            else
                return ImageFormat.Wmf;
        }

        #endregion Image helpers

        private void removeItemButton_Click(object sender, EventArgs e)
        {
            if (SelectedItem != null)
            {
                StorageAliases.Remove(SelectedItem);
                PopulateListBox(aliasListBox, StorageAliases);
                aliasTextBox.Text = string.Empty;
                pathTextBox.Text = string.Empty;
                imagePictureBox.Image = null;
                SelectedItem = null;
            }
        }

        private void clearAllButton_Click(object sender, EventArgs e)
        {
            StorageAliases.Clear();
            aliasListBox.Items.Clear();
            aliasTextBox.Text = string.Empty;
            pathTextBox.Text = string.Empty;
            imagePictureBox.Image = null;
            SelectedItem = null;
        }
    }
}