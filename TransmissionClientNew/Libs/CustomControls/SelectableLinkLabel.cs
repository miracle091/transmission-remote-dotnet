﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet
{
    internal class SelectableLinkLabel : RichTextBox
    {
        public SelectableLinkLabel()
        {
            BorderStyle = BorderStyle.None;
            ReadOnly = true;
            base.Text = "";
            Visible = false;
#if !MONO
            MouseUp += new MouseEventHandler(
                delegate (object sender, MouseEventArgs e) { HideCaret((sender as Control).Handle); }
            );
#endif
        }

#if !MONO

        [DllImport("User32.dll")]
        private static extern bool HideCaret(IntPtr hWnd);

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                Visible = value.Length > 0;
            }
        }

#endif
    }
}