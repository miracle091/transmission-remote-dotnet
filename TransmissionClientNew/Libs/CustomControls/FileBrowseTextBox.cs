﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet.CustomControls
{
    /// <summary>
    /// Summary description for SkinImageBrowseTextBox.
    /// </summary>
    public class SkinImageBrowseTextBox : UserControl
    {
        private TextBox fileTextBox;
        private Button browseButton;
        private GroupBox groupBox;
        public PictureBox pictureBox;
        private Panel PicturePanel;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public SkinImageBrowseTextBox()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            fileTextBox = new TextBox();
            browseButton = new Button();
            groupBox = new GroupBox();
            PicturePanel = new Panel();
            pictureBox = new PictureBox();
            groupBox.SuspendLayout();
            PicturePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox)).BeginInit();
            SuspendLayout();
            //
            // fileTextBox
            //
            fileTextBox.Anchor = ((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            fileTextBox.Location = new Point(6, 19);
            fileTextBox.Name = "fileTextBox";
            fileTextBox.Size = new Size(258, 20);
            fileTextBox.TabIndex = 0;
            fileTextBox.Text = "fileTextBox";
            fileTextBox.TextChanged += new EventHandler(fileTextBox_TextChanged);
            //
            // browseButton
            //
            browseButton.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            browseButton.Location = new Point(270, 18);
            browseButton.Name = "browseButton";
            browseButton.Size = new Size(24, 21);
            browseButton.TabIndex = 1;
            browseButton.Text = "...";
            browseButton.Click += new EventHandler(browseButton_Click);
            //
            // groupBox
            //
            groupBox.Controls.Add(PicturePanel);
            groupBox.Controls.Add(browseButton);
            groupBox.Controls.Add(fileTextBox);
            groupBox.Dock = DockStyle.Fill;
            groupBox.Location = new Point(0, 0);
            groupBox.Name = "groupBox";
            groupBox.Size = new Size(300, 100);
            groupBox.TabIndex = 0;
            groupBox.TabStop = false;
            groupBox.Text = "groupBox1";
            //
            // PicturePanel
            //
            PicturePanel.Anchor = (((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
            PicturePanel.AutoScroll = true;
            PicturePanel.Controls.Add(pictureBox);
            PicturePanel.Location = new Point(6, 45);
            PicturePanel.Name = "PicturePanel";
            PicturePanel.Size = new Size(288, 49);
            PicturePanel.TabIndex = 2;
            //
            // pictureBox
            //
            pictureBox.Location = new Point(0, 0);
            pictureBox.Name = "pictureBox";
            pictureBox.Size = new Size(32, 32);
            pictureBox.TabIndex = 2;
            pictureBox.TabStop = false;
            //
            // SkinImageBrowseTextBox
            //
            Controls.Add(groupBox);
            MinimumSize = new Size(0, 68);
            Name = "SkinImageBrowseTextBox";
            Size = new Size(300, 100);
            groupBox.ResumeLayout(false);
            groupBox.PerformLayout();
            PicturePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(pictureBox)).EndInit();
            ResumeLayout(false);
        }

        #endregion Component Designer generated code

        private void browseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = OtherStrings.OpenImageFilter;
            openFile.RestoreDirectory = true;
            openFile.Multiselect = true;
            if (openFile.ShowDialog() == DialogResult.OK)
                fileTextBox.Text = openFile.FileName;
        }

        private void fileTextBox_TextChanged(object sender, EventArgs e)
        {
            pictureBox.Image = Toolbox.LoadSkinImage(fileTextBox.Text, MinHeight, MaxHeight, ImageNumber);
            if (pictureBox.Image != null)
            {
                pictureBox.Width = pictureBox.Image.Width;
                pictureBox.Height = pictureBox.Image.Height;
            }
            else
            {
                pictureBox.Width = 1;
                pictureBox.Height = 1;
            }
        }

        public string Title
        {
            get { return groupBox.Text; }
            set { groupBox.Text = value; }
        }

        public string FileName
        {
            get { return fileTextBox.Text; }
            set { fileTextBox.Text = value; }
        }

        public int MaxHeight { get; set; }
        public int MinHeight { get; set; }
        public int ImageNumber { get; set; }
    }
}