﻿using System.Windows.Forms;

namespace TransmissionRemoteDotnet.CustomControls
{
    internal class USButton : Button
    {
        public USButton() : base()
        {
            SetStyle(ControlStyles.Selectable, false);
        }
    }
}