﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet
{
    public class InputBox
    {
        private class InputBoxForm : Form
        {
            private Label lblPrompt;
            private Button btnOK;
            private Button btnCancel;
            private TextBox txtInput;

            public InputBoxForm()
            {
                InitializeComponent();
            }

            private void InitializeComponent()
            {
                lblPrompt = new Label();
                btnOK = new Button();
                btnCancel = new Button();
                txtInput = new TextBox();
                SuspendLayout();
                //
                // lblPrompt
                //
                lblPrompt.Location = new Point(12, 9);
                lblPrompt.Name = "lblPrompt";
                lblPrompt.Size = new Size(302, 13);
                lblPrompt.TabIndex = 0;
                //
                // btnOK
                //
                btnOK.DialogResult = DialogResult.OK;
                btnOK.Location = new Point(226, 70);
                btnOK.Name = "btnOK";
                btnOK.Size = new Size(64, 24);
                btnOK.TabIndex = 2;
                btnOK.Text = "&OK";
                //
                // btnCancel
                //
                btnCancel.DialogResult = DialogResult.Cancel;
                btnCancel.Location = new Point(326, 70);
                btnCancel.Name = "btnCancel";
                btnCancel.Size = new Size(64, 24);
                btnCancel.TabIndex = 3;
                btnCancel.Text = OtherStrings.Cancel;
                //
                // txtInput
                //
                txtInput.Location = new Point(16, 30);
                txtInput.Name = "txtInput";
                txtInput.Size = new Size(369, 20);
                txtInput.TabIndex = 1;
                //
                // InputBox
                //
                AcceptButton = btnOK;
                AutoScaleBaseSize = new Size(5, 13);
                CancelButton = btnCancel;
                ClientSize = new Size(398, 108);
                Controls.Add(txtInput);
                Controls.Add(btnCancel);
                Controls.Add(btnOK);
                Controls.Add(lblPrompt);
                DoubleBuffered = true;
                FormBorderStyle = FormBorderStyle.FixedDialog;
                MaximizeBox = false;
                MinimizeBox = false;
                Name = "InputBox";
                Text = "InputBox";
                Load += new EventHandler(InputBox_Load);
                StartPosition = FormStartPosition.CenterScreen;
                ShowInTaskbar = false;
                ResumeLayout(false);
                PerformLayout();
            }

            private void InputBox_Load(object sender, EventArgs e)
            {
                BringToFront();
                txtInput.Focus();
                txtInput.SelectionLength = txtInput.Text.Length;
            }

            #region Private Properties

            internal string FormCaption
            {
                set
                {
                    Text = value;
                }
            } // property FormCaption

            internal string FormPrompt
            {
                set
                {
                    lblPrompt.Text = value;
                }
            } // property FormPrompt

            internal string Value
            {
                get
                {
                    return txtInput.Text;
                }
                set
                {
                    txtInput.Text = value;
                }
            } // property Value

            internal bool UseSystemPasswordChar
            {
                set
                {
                    txtInput.UseSystemPasswordChar = value;
                }
            } // property UseSystemPasswordChar

            #endregion Private Properties
        }

        static public string Show(string Prompt)
        {
            return Show(Prompt, string.Empty);
        }

        static public string Show(string Prompt, string Title)
        {
            return Show(Prompt, Title, string.Empty);
        }

        static public string Show(string Prompt, string Title, bool UseSystemPasswordChar)
        {
            return Show(Prompt, Title, string.Empty, UseSystemPasswordChar);
        }

        static public string Show(string Prompt, string Title, string Default)
        {
            return Show(Prompt, Title, Default, false);
        }

        static public string Show(string Prompt, string Title, string Default, bool UseSystemPasswordChar)
        {
            InputBoxForm frmInputDialog = new InputBoxForm();
            frmInputDialog.FormCaption = Title;
            frmInputDialog.FormPrompt = Prompt;
            frmInputDialog.Value = Default;
            frmInputDialog.UseSystemPasswordChar = UseSystemPasswordChar;

            return frmInputDialog.ShowDialog() == DialogResult.OK ? frmInputDialog.Value : null;
        }
    }
}