﻿using Jayrock.Json;
using MaxMind;
using System;
using System.Net;
using System.Windows.Forms;

namespace TransmissionRemoteDotnet
{
    public class PeerListViewItem : ListViewItem
    {
        public static int CurrentUpdateSerial = 0;

        public void Update(JsonObject peerObj)
        {
            FlagStr = (string)peerObj[ProtocolConstants.FIELD_FLAGSTR];
            Progress = Toolbox.ToProgress(peerObj[ProtocolConstants.FIELD_PROGRESS]);
            if (Program.DaemonDescriptor.Trunk && Program.DaemonDescriptor.Revision >= 10937 && Program.DaemonDescriptor.Revision < 11194)
            {
                RateToClient = (long)(Toolbox.ToDouble(peerObj[ProtocolConstants.FIELD_RATETOCLIENT]) * 1024);
                RateToPeer = (long)(Toolbox.ToDouble(peerObj[ProtocolConstants.FIELD_RATETOPEER]) * 1024);
            }
            else
            {
                RateToClient = Toolbox.ToLong(peerObj[ProtocolConstants.FIELD_RATETOCLIENT]);
                RateToPeer = Toolbox.ToLong(peerObj[ProtocolConstants.FIELD_RATETOPEER]);
            }
        }

        public PeerListViewItem(JsonObject peerObj) : base((string)peerObj[ProtocolConstants.ADDRESS])
        {
            for (int i = 0; i < 7; i++)
                SubItems.Add("");

            Address = Name = Text;
            int countryIndex = -1;
            if (!GeoIPCountry.Disabled)
            {
                try
                {
                    countryIndex = GeoIPCountry.Instance.FindIndex(IpAddress);
                }
                catch { }
            }
            if (countryIndex > 0)
                Country = GeoIPCountry.CountryNames[countryIndex];

            ClientName = (string)peerObj[ProtocolConstants.FIELD_CLIENTNAME];
            Update(peerObj);
            if (countryIndex > 0)
                ImageIndex = GeoIPCountry.FlagImageList.Images.IndexOfKey("flags_" + GeoIPCountry.CountryCodes[countryIndex].ToLower());

            Dns.BeginGetHostEntry(IpAddress, new AsyncCallback(GetHostEntryCallback), this);
        }

        private delegate void SetHostNameDelegate(IPHostEntry host);

        private void SetHostName(IPHostEntry host)
        {
            if (ListView != null && ListView.InvokeRequired)
                ListView.Invoke(new SetHostNameDelegate(SetHostName), host);
            else if (host != null && !host.HostName.Equals(SubItems[0].Text))
                Hostname = host.HostName;
        }

        private static void GetHostEntryCallback(IAsyncResult ar)
        {
            PeerListViewItem item = (PeerListViewItem)ar.AsyncState;
            try
            {
                IPHostEntry host = Dns.EndGetHostEntry(ar);
                item.SetHostName(host);
            }
            catch
            {
            }
        }

        private void SetText(int idx, string str)
        {
            if (!str.Equals(SubItems[idx].Text))
                SubItems[idx].Text = str;
        }

        public string Hostname
        {
            get { return SubItems[1].Text; }
            set { SetText(1, ToolTipText = value); }
        }

        public int UpdateSerial
        {
            get;
            set;
        }

        public string Address
        {
            get
            {
                return Text;
            }
            set
            {
                Text = ToolTipText = value;
                IpAddress = IPAddress.Parse(value);
            }
        }

        public IPAddress IpAddress
        {
            get;
            set;
        }

        public decimal Progress
        {
            get
            {
                return (decimal)SubItems[5].Tag;
            }
            set
            {
                SetText(5, value + "%");
                SubItems[5].Tag = value;
            }
        }

        public long RateToPeer
        {
            get
            {
                return (long)SubItems[7].Tag;
            }
            set
            {
                SetText(7, Toolbox.GetSpeed(value));
                SubItems[7].Tag = value;
            }
        }

        public long RateToClient
        {
            get
            {
                return (long)SubItems[6].Tag;
            }
            set
            {
                SetText(6, Toolbox.GetSpeed(value));
                SubItems[6].Tag = value;
            }
        }

        public string Country
        {
            get { return SubItems[2].Text; }
            set { SetText(2, value); }
        }

        public string FlagStr
        {
            get { return SubItems[3].Text; }
            set { SetText(3, value); }
        }

        public string ClientName
        {
            get { return SubItems[4].Text; }
            set { SetText(4, value); }
        }
    }
}