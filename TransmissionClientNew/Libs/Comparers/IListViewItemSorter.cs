﻿using System.Windows.Forms;

namespace TransmissionRemoteDotnet.Comparers
{
    internal interface IListViewItemSorter
    {
        int SortColumn { get; set; }
        SortOrder Order { get; set; }
    }
}